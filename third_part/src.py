import requests
import json
import csv
#excercise 1 
#no explanation needed just sending a post request
def http_request(parameter):
    URL = "https://httpbin.org/anything"

    response = requests.post(url = URL, params = parameter)
    #print(response)
    data = response.json()
    json_string = json.dumps(data)
    print(json_string)



http_request({'User-Agent':'Ahmed'})
http_request({'isadmin':1})




#excercise 2
#i made just a function instead of a class because i  had several problems
#this function do all what was asked 
def readJson():
    file = open('test.csv','w')
    file = csv.writer(file,delimiter='\t')
    #f = open('data.json')
    f = open('data/data.json')
    data = json.load(f)
    key = "Product"
    #till here we just creat a csv file and load the json data
    for i, product in enumerate(data["Bundles"]):
        try:
            #here we start looping through the json to get the names and prices of the prodcuts
            product_name = product["Name"]
            try:
                for infos in product[key]:
                    if(infos["IsAvailable"]):
                        product_price = infos["Price"]
                    else:
                        print(f"This product is unavailable ID:{i} {product['Name'][:30]}")
#here the json was a bit complicated to deal with.the tags were not all the same for example:
#there are Product and Products with and without "s" so we catch these error so we can change them and deal with them
            except KeyError:
                key = "Products"
            finally:
#if we find an available product we add it to the csv file
                print(f"You can buy {product_name[:30]} at out store at {str(round(product_price, 2))}")
                file.writerow(product_name[:30])
        except KeyError:
            print("The Product can't be found") 
        finally:
            continue
        file.close()

#for test
readJson()



