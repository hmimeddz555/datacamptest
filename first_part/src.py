#this excercise is straightforward i think no explenation is needed
def excercise_one():
    for i in range(1,101):
        if i % 3 == 0 and i % 5 == 0:
            print("three-Five")
        elif i % 3 == 0:
            print("three")

        elif i % 5 == 0:
            print("Five")

        elif i % 3 == 0:
            print("three")
        else:
            print(i)

#the idea for this excercise is that we muliply each number with the upcoming nombers of the list of
#ex: [1,2,3,4] => [1.2, 1.2.3, 2.3, 2.3.4]
def excercise_two(number):
    #list of muliplication
    product = []
    #here we separate the number (250)=>[2,5,0]
    list = [int(a) for a in str(number)]
    for i,j in enumerate(list):
        k = 0
        if i == (len(list)-1):
            break
        mult = j*list[i+1]
        product.append(mult)
        #print(mult)
        if i+1 == len(list):
            product.append(mult)
            break
        k = 2+i
        #till here i took the cases where the itterator doesnt get pass the list
        #or when the itterator reach the final index of the list we break
        while(k<=(len(list)-1)):
            mult = mult*list[k]
            product.append(mult)
            k+=1
        #here we just multiply each number with the rest of the list numbers
    product.extend(list)
    #i just added the numbers that i recently separated so they dont get multiplied
    if len(product) == len(set(product)):
        #if the  lists are equalls that means there is no duplicate and thus the number is colorful
        return True
    else:
        return False



def excercise_three(list):
    list_of_numbers = []
    for item in list:
        #here we only need to take numbers that are in string format
        #we filter words and numbers not in string format
        if  isinstance(item, str) == False or bool(re.search(r'\d', item)) == False: 
            pass
        else:
            try:
                #here we deal with the negative numbers because they cant be stored as int
                list_of_numbers.append(int(item))
            except ValueError as e:
                list_of_numbers.append(item)
    return sum(list_of_numbers)
   

#the idea is straighforward we just compare the word with the sorted list of words
def excercise_four(word,list):
    anagrams = []
    for i in list:
         if(sorted(i)== sorted(word)):
             anagrams.append(i)

    print(anagrams)
    return anagrams