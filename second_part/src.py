#Excercise 1
#no explanation needed
def Random_gen():
    num = 0
    while (num != 15):
        num = random.randint(10, 15)
        yield num


#excercise 2 
#this decorator just return the value as strings
#it takes either the addition of 2 numbers or a dictionairy with int values
def decorator_to_str(func):
    # todo exercise 2
    def inner(*args, **kwargs):
        val = func(*args, **kwargs)
        return str(val)
    
    return inner 


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']


#excercise 3
#as a decorator with parameters we need another decorator to handle the div error
#and the function div to be executed
def ignore_exception(exception):  
    def dev(func):
        def inner(*args, **kwargs):
            try:
                val = func(*args)
                return val
            except exception:
                return None    
        
        return inner
    return dev


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap



#excercise 5
#this meta class make sure that other classes must inherit from List 
#it checks the bases if it of type list or not
class MetaInherList(type):
    def __new__(self, class_name, bases, attrs):
        try:
            if(bases[0] == list):
                print(f"The class {class_name} inherit from list class")
                return type(class_name, bases, attrs)
        except IndexError:
            print(f"The class {class_name} should only inherit from list class")
           
        else:
            print(f"The class {class_name} should only inherit from list class")


class ForceToList(list, metaclass=MetaInherList):

    def __init__(self,list):
        self.list = list

    def test_list(self):
        print(self.list[0])
        print(self.list[1])
        self.list.append(5)
        return self.list


#excercise6
#meta class to check if a class has a process attribute 
#we can do that by verifying the attribute in the meta class
# we can just test the class by creating an object if the class has process attribute
#then the object will be created if not their will be a messege error telling you their is no process in the class
class MetaCheck(type):
    def __new__(self, class_name, bases, attrs):
        for key, val in attrs.items():
                if key == "process":
                    print(f"the class {class_name} has the attribute process")
                    #Return the class 
                    return type(class_name, bases, attrs)

                else:
                    continue
        print(f"the class {class_name} does not have the process attribute")                    
        

class CheckAttr(metaclass=MetaCheck):
    process = "foo"
    def hello(self):
        print("i'm using meta class")

obj = CheckAttr()